import pytest
import source.day_1_trebuchet as day_1_trebuchet


def test_day_1_part_1():
    file_location = "tests\data\day1_1.txt"
    # Run the function and save the return value
    result = day_1_trebuchet.day_1(file_location = file_location)
    # Assert the value agains what I would expect
    assert result == 142

def test_day_1_part_2():
    file_location = "tests\data\day1_2.txt"
    # Run the function and save the return value
    result = day_1_trebuchet.day_1(file_location = file_location)
    # Assert the value agains what I would expect
    assert result == 281
