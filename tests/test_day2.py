import pytest
import source.day_2_cubeconundrum as day_2_cubeconundrum


def test_day_2_part_1():
    file_location = "tests\data\day2_1.txt"
    # Run the function and save the return value
    possible_games_sum, x = day_2_cubeconundrum.day_2(file_location = file_location)
    # Assert the value agains what I would expect
    assert possible_games_sum == 8
    
def test_day_2_part_2():
    file_location = "tests\data\day2_1.txt"
    # Run the function and save the return value
    x, minimum_viable_games_sum = day_2_cubeconundrum.day_2(file_location = file_location)
    # Assert the value agains what I would expect
    assert minimum_viable_games_sum == 2286