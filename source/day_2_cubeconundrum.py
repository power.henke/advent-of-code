import re

def day_2(file_location):
    """This function will take each line of the file and find out if the game was possible or not as well as find the minimum viable game.

    Args:
        file_location (str): Location of a textfile containing the input for the challenge. 
    """
    file_location = file_location
    
    # Game rules
    max_red = 12
    max_green = 13
    max_blue = 14

    all_games = []
    impossible_games = []
    minimum_viable_games = []
    

    with open(file_location) as file:
        
        # Get all matches, one per line
        for line in file:
            # Get the game number
            match = line.split(":")
            game_number = int(match[0].strip("Game "))
            all_games.append(game_number)
            match = match[-1].strip("\n").split("; ")
            
            red_count = []
            green_count = []
            blue_count = []
            
            # Check if any game was impossible
            for game in match:    
                game = game.rstrip().lstrip()
                game = game.split(", ")
                
                # Check the scores in each round in each game
                for score in game:
                    # Save all states in each game
                    if "red" in score:
                        red_count.append(int(re.sub("\\D", "", score)))
                    if "green" in score:
                        green_count.append(int(re.sub("\\D", "", score)))
                    if "blue" in score:
                        blue_count.append(int(re.sub("\\D", "", score)))

                # Check if the game was possible or not
                for score in game:
                    if "red" in score and int(re.sub("\\D", "", score)) > max_red:
                        impossible_games.append(game_number)
                        break
                    elif "blue" in score and int(re.sub("\\D", "", score)) > max_blue:
                        impossible_games.append(game_number)
                        break
                    elif "green" in score and int(re.sub("\\D", "", score)) > max_green:
                        impossible_games.append(game_number)
                        break

            minimum_viable_games.append(max(red_count) * max(blue_count) * max(green_count))
            
            # print("Red:", red_count)
            # print("Blue:", blue_count)
            # print("Green:", green_count)
            # print("\n")
            
    
    # print(minimum_viable_games)
            
    # Take all games and remove the impossible games to get the possible games
    possible_games = [i for i in all_games if i not in impossible_games]

    return(sum(possible_games), sum(minimum_viable_games))

possible_games_sum, minimum_viable_games_sum = day_2(file_location = "data\\day2_1.txt")
print(possible_games_sum, minimum_viable_games_sum)
