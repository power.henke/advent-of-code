# In this challenge, the problem that needs to be solved is that I will get a long list of strings contaitning numbers and letters. I need to strip out all letters, and then add together all numbers that I have left on each row. 
import re


def day_1(file_location):
    """This function will take the file at the given location, look through it line by line, remove ant letters from each line and treat the remaining numbers as one, and then add all numbers from each line together to get a final number.

    Args:
        file_location (str): Location of a textfile containing the input for the challenge. 
    """
    file_location = file_location

    # Replacement map for the second task of day 1
    replace = {"one" : "o1e",
               "two" : "t2o",
               "three" : "t3e",
               "four" : "f4r",
               "five" : "f5e",
               "six" : "s6x",
               "seven" : "s7n",
               "eight" : "e8t",
               "nine" : "n9e"}
    
    numbers_list = []
    
    with open(file_location) as file:
        for line in file:
            # Look for the words corresponding to numbers and replace them
            for i, j in replace.items():
                line = line.replace(i, j)
            print(line)
            # Remove all non-numbers using re
            long = (re.sub('\\D', '', line))
            
            # Get the first and last item in the list, if there is only one number it will be selected twice
            short = long[0] + long[-1]
            
            # Append to the list, make the numbers integers
            numbers_list.append(int(short))
    
    # print(numbers_list)
    # print(sum(numbers_list))
    return(sum(numbers_list))

result = day_1("data\\day1_2.txt")
print(result)
